/** main.typ
 *
 * The entrypoint of this exercise set, that should be compiled.
 *
***/

#import "exercisetemplate.typ"

#show: doc => exercisetemplate.exercisetemplate(
	coursecode : "A.B.C",
	title : "Exercise solutions",
	keywords : (
		"exercises",
	),
	authors : (
		(
			firstname : "Firstname",
			lastname : "Lastname"
		),
	),
	doc
)

//// Write content here.

#include("exercise1.typ")
#include("exercise2.typ")
#include("exercise3.typ")
#include("exercise4.typ")
#include("exercise5.typ")
