# typst Exercise Template

This is a very simple exercise template written in the [`typst`][typst]
typesetting language. To compile this example document locally, run
```sh
typst compile main.typ
```
in the shell of your choice. If you wish that `typst` automatically
tracks changes to your project and compiles it automatically as it
detects changes, use
```sh
typst watch main.typ
```
instead. This is how the online typst platform at <https://typst.app/>,
where you can upload this project, performs automatic compilation. See
the tutorial at <https://typst.app/docs/tutorial/> to learn the basics.

**Note:** as of version `0.10.0`, the `typst watch` functionality does
not yet support placing project-related files into subfolders. Therefore
all chapter-specific typst files, images and code snippets need to be
placed in the same folder with `main.typ`.

[typst]: https://github.com/typst/typst

## License

This project itself uses the MIT license. See the
[LICENSE](./LICENSE) file for details.
