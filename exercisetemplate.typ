/** tauthesis.typ
 *
 * This module defines the Tampere University thesis template structure. You
 * should not need to touch this file. Define your own commands in preamble.typ
 * instead.
 *
***/

//// Constants

#let templatename = "Exercise template"

#let finnish = "fi"

#let english = "en"

/** titlepage(title, authors, examiners, submissiondate)
 *
 * Defines the look of the title page, and returns the content related to it.
 *
 **/

#let titletemplate(
	title,
	authors,
	coursecode,
) = {
	set align(center)
	block(text(20pt, coursecode))
	v(1.5em, weak:true)
	block(text(25pt, title))
	let count = authors.len()
	let ncols = calc.min(count, 3)
	grid(
		columns: (1fr,) * ncols,
		row-gutter: 24pt,
		..authors.map( author => [
			#set text(size:15pt)
			#author.firstname
			#author.lastname
		] ),
	)
	v(1.5em, weak:true)
	set text(size:13pt)
	datetime.today().display("[day]/[month]/[year]")
	line(
		length: 100%,
		stroke: 2pt + black,
	)
}

#let CODEBLOCKKIND = "CODEBLOCKKIND"

#let codeblock(
	caption : [],
	tab-size : 4,
	language : "text",
	filepath,
) = figure(
	align(
		left,
		raw(
			read(filepath),
			block: true,
			lang: language,
			align: left,
			syntaxes: (),
			tab-size: tab-size,
		),
	),
	kind: CODEBLOCKKIND,
	caption: caption,
	supplement: supplement,
)

/** theoremtemplate
 *
 * Defines the basic shape of Tampere University mathematics theorem blocks.
 * Below are also defined some common mathematical theorem blocks.
 *
 **/

#let theoremcounter = counter("theorem")

#let THEOREMKIND = "THEOREMKIND"

#let theoremtemplate(
	fill: rgb("#ffffff"),
	supplement: [Theorem Block],
	title: [],
	content
) = figure(
	kind: THEOREMKIND,
	supplement: supplement,
	block(
		fill: fill,
		inset: 8pt,
		radius: 4pt,
		stroke: black,
		width: 100%,
		breakable: true,
		align(
			left,
			[
				#theoremcounter.step()
				#text(weight: "bold", fill: black, [#supplement #theoremcounter.display()])
				// Content converted to string with repr always has a lenght ≥ 2.
				#if repr(title).len() > 2 [
					(#text(weight: "bold", fill: black, title))
				]
				#content
			],
		)
	)
)

#show figure.where( kind: THEOREMKIND ): it => it

#let definition(title: "", content) = theoremtemplate(
	supplement: "Definition",
	title: title,
	content
)

#let lemma(title: "", content) = theoremtemplate(
	supplement: "Lemma",
	title: title,
	content
)

#let theorem(title: "", content) = theoremtemplate(
	supplement: "Theorem",
	title: title,
	content
)

#let corollary(title: "", content) = theoremtemplate(
	supplement: "Corollary",
	title: title,
	content
)

#let example(title: "", content) = theoremtemplate(
	supplement: "Example",
	title: title,
	content
)

#let määritelmä(title: "", content) = theoremtemplate(
	supplement: "Määritelmä",
	title: title,
	content
)

#let apulause(title: "", content) = theoremtemplate(
	supplement: "Apulause",
	title: title,
	content
)

#let lause(title: "", content) = theoremtemplate(
	supplement: "Lause",
	title: title,
	content
)

#let seurauslause(title: "", content) = theoremtemplate(
	supplement: "Seurauslause",
	title: title,
	content
)

#let esimerkki(title: "", content) = theoremtemplate(
	supplement: "Esimerkki",
	title: title,
	content
)

/** exercisetemplate
 *
 * Defines the structure of this exercise template.
 *
 **/

#let exercisetemplate(
	authors : (
		(
			firstname : "Firstname",
			lastname  : "lastname"
		),
	),
	title : "Title",
	keywords : ("exercises",),
	language : english,
	textfont : ("Helvetica", "Fira Sans", "DejaVu Sans"),
	mathfont : ("Euler Math", "New Computer Modern Math"),
	textfontsize : 12pt,
	mathfontsize : 12pt,
	codefontsize : 10pt,
	headingsep : 2 * " ",
	codeblockinset : 0.5em,
	coursecode : "A.B.C",
	doc
) = {

	if not language in (finnish, english) {
		panic( templatename + ": unrecognized primary language. Only 'fi' and 'en' are accepted." )
	}

	set document(
		author: authors.map(aa => aa.firstname + " " + aa.lastname),
		title: (coursecode, title).join(" : "),
		keywords: keywords.join(", "),
		date: datetime.today(),
	)

	show figure.where( kind: CODEBLOCKKIND ): set figure.caption(position: top)

	show heading: it => {
		if it.level > 3 { panic("Only headings up to level 3 are allowed. Please reconsider your document structure.") }
		set block( above: 1.4em, below: 2em )
		it
	}

	show raw: set text(size: codefontsize)

	show raw.where( block : false ) : it => {
		box(
			height : 1.1 * fontsize,
			inset : 0.2 * codefontsize,
			fill : luma(230),
			baseline : 0.3 * codefontsize,
			radius : 0.3 * codefontsize,
			it
		)
	}

	// Number code block lines.

	show raw.where( block : true ): it => {
		let codelines = it.lines
		let linenumbers =  codelines.map(line => line.number)
		let rawnumstr = raw(linenumbers.map(nn => str(nn)).join("\n"))
		let numberblock = block(
			width : 100%,
			inset : codeblockinset,
			align(
				right,
				rawnumstr,
			)
		)
		grid(
			columns : (5%, 95%),
			numberblock,
			align(
				left,
				block(
					width: 100%,
					fill: luma(245),
					inset: codeblockinset,
					it
				)
			)
		)
	}

	set text( font: textfont, lang: language, size: textfontsize )

	titletemplate( title, authors, coursecode )

	outline( depth : 2 )

	show heading.where(level: 1): it => [
		#pagebreak(weak: true)
		#it
	]

	show heading.where( level : 3 ): it => [
		#it.body
	]

	show figure.where( kind: table ): set figure.caption(position: top)

	show math.equation: set text(font: mathfont, size: mathfontsize)

	set page(
		numbering: "1",
		header: locate(
			loc => {
				let authortext = {
					let firstauthor = authors.first()
					let faname = [#firstauthor.firstname #firstauthor.lastname]
					if authors.len() > 1 {
						[#faname et al.]
					} else {
						[#faname]
					}
				}
				let headselector = selector(
					heading
					.where( outlined : true )
					.and(
						heading
						.where( level : 1 )
						.or( heading.where( level : 2) )
					)
				)
				let beforeelems = query( headselector.before(loc), loc,)
				let afterelems = query( headselector.after(loc), loc,)
				let currentheadingquery = query( headselector, loc)
				let currentheading = currentheadingquery.find( h => h.location().page() == loc.page())
				let lefttext = smallcaps[
					#box(
					[#coursecode \ #title]
					)
				]
				let righttext = box[
					#if not currentheading == none {
						emph(currentheading.body)
						[ ]
						counter(heading).at(currentheading.location()).map(str).join(".")
					} else if not beforeelems == () {
						emph[
							beforeelems.last().body
							#counter(heading).at(beforeelems.last().location()).map(ii => str(ii)).join(".")
						]
					} else if not afterelems == () {
						afterelems.first().body
					} else {
						[]
					} \ #authortext
				]
				lefttext + h(1fr) + righttext
				line(length : 100%, stroke : 2pt + black)
			}
		),
		footer: {
			set align(center)
			counter(page).display()
		},
		margin: (
			top: 3cm,
			bottom: 2cm,
			left: 2cm,
			right: 2cm,
		)
	)

	set math.vec(delim : "[")

	set math.mat(delim : "[")

	set math.equation(
		numbering: nn => {
			set heading( numbering: "1." )
			"(" + counter(heading).display() + str(nn) + ")"
		},
		supplement: none
	)

	set par( leading: 0.55em, first-line-indent: 1.8em, justify: true )

	show par: set block(spacing: 0.55em)

	set heading( numbering: "1.1" + headingsep, outlined: true, supplement: none )

	counter(page).update( 1 )

	doc

}

#let appendix(doc) = {

	counter(heading).update( 0 )

	counter(math.equation).update( 0 )

	set heading(numbering: "A.1" + headingsep, supplement: none)

	set math.equation(
		numbering: nn => {
			set heading( numbering: "A." )
			"(" + counter(heading).display() + str(nn) + ")"
		},
		supplement: none
	)


	doc

}
